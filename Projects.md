Vagrant Details | Installing and Configuring Jenkins | Projects |

# Projects

# Part 1 Creating a freestyle project

1. Create a new item in the jenkins main dashboard.
2. Select free style project and give it a name.
3. You will be presented with a blank slate.
4. Here you can configure build steps, Source repository and plugin steps
5. If you are unsure what an option does check the question mark.

# Part 2 Configuring source code management

## Prerequisites.

0. Ensure the relevant source code is installed on the slave/master you want to use. E.g apt-get install git

## Configuring scm

0. Ensure connectivity between your scm server and jenkins
1. Inside of your freestyle proejct scroll down to the source code management steps.
2. Git is bundled in the suggested plugins you can add SVN or other scm systems via plugin if needed.
3. Add the URL via http or ssh.
4. If the latter ensure the slave can talk to the scm server by adding the relevant ssh key
5. store the ssh key or http auth creds as a jenkins credentials and assign it to the cred tab.
6. Configure refspec if needed.
7. You can specify a specific branch to build via the branch field.

## Configuring build triggers.

1. You can configure triggers based on what is happening in the git project.
2. Poll SCM puts load on jenkins to poll the repo for changes. You can set this via cron
3. Trigger remote is an external call to jenkins to start the build
4. Build after other projects creates a dependancy on another build finishing. 

## Build Environment

1. Here you can configure specific build environment procedues such as timestamping failutres

## Build Steps

1. Here you can add commands for either deploying or automating anything to do with your source code.
2. These can be configured and added with plugins. e.g. Maven or MSbuid plugin.

# Part 3 githooks and build triggers

## git hooks with the gitplugin/gitlab plugin

1. In the freestyleproject scroll down to build triggers
2. Select your relevant scm trigger. e.g. as im using gitlab for me its build when a change is pushed to gitlab
3. On your repo in gitlab go down to repository settings and click the Jenkins CI
4. Fill in the jenkins url, the project name in jenkins, and the user which it will run as
5. Ensure the gitlab server can talk to jenkins 
6. When pushing to this repo it should automatically trigger the build in jenkins
7. The same steps are true for github integraion

## Creating a webhook to trigger jenkins

1. Gitlab EE has an integrated Jenkins CI plugin. But gitlab CE doesnt
2. To fix this you can download the generic webhooks plugin and configure a webhook yourself.
3. Disable CSRF in global security
4. In the build triggers create a token by ticking trigger build remotely and also select the webhook plugin
5. In gitlab under integrations select webhook and add the following url http://JENKINSURL/generic-webhook-trigger/invoke?token=(yourtoken)

# Part 4 Workspace Environment variables

## Standard environment variables

1. $BUILD_NUMBER
2. $NODE_NAME
3. $JOB_NAME
4. $EXECUTOR_NUMBER
5. $WORKSPACE

## Git specific environment variables

1. $GIT_COMMIT = hash of current commit
2. $GIT_BRANCH = current built branch
3. $GIT_PREVIOUS_COMMIT = hash of previous commit
4. $GIT_URL =  clonable git url

# Part 5 Parameterised projects

1. Under General make sure you check this project is parameterised
2. Once ticked you can add parameters to your build

## Important types

* String = string parameter
* File = reference to a file
* Bool = Boolean
* Choice = multi select parameters
* Credentials = reference to a credential
* Password = Protected in configuration but not the console output
* Run = Accessing values for a specific build can access via $variable.param

3. These paramters can be referend throughout your buildsteps and configuration for your projects

# Part 6 Upstream/Downstream Projects and the Parameterised Trigger plugin

* Downstream project triggered by the completion of the upstream project
* The upstream project is the project that triggers the downstream project

1. In your downstream project select build triggers and check build after project completed
2. In this check box use the upstream project as the reference name.

## Paramterised trigger plugin

You can use this to pass a paramter from the upstream project to the downstream project

1. Install the paramterized trigger plugin from manage plugins
2. In your upstream project add a build step called trigger call builds from other projects
3. In this reference your downstream project and add a paramter. If you have a parameter predefined on your downstream you can select downstream parameter here.
4. You can remove the build trigger in the reference of the downstream project as it will run twice. The build step in the upstream project is now triggering the downstream project rather then vice versa.

# Part 7 Folders

To add folder to organise your projects you click new item at the main dashboard

* Display Name = Name displayed as in Jenkins
* Health Metrics = Check current projects over all health and show it on folder

You can move exsisting jobs into a folder by clicking the drop down next to each project

You can also do this on the project main screen by select move there.

# Part 8 Views

## My Views

1. The default View is the all View and its presented when you first log into Jenkins
2. To add a view click the ltitle plus near the all tab.
3. To main types of view list view and my view. The my view list all jobs the current user has access to.
4. You can click edit view to filter the view on build queue and executor

## List Views

1. Similar creation to My View
2. When editing and creating this view you can filter on certain jobs such as enabled jobs or disabled jobs.
3. By selecting Recurse you can view stuff inside of folders. You can alos use regex for this filter. e.g. ."*GitLab*"
4. You can add filters and colums to help set the information displayed in your views.
5. Views can be extended greatly by certain plugins.

