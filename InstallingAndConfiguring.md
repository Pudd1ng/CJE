Vagrant Details | Installing and Configuring Jenkins | Projects |

# Installing and Configuring Jenkins

# Part 1 CI and CD

1. Continuous Deployment = A software development discipline where software is released continuously as part of an automated pipeline.
2. Continuous Delivery = A software development discipline where software is built so that it can be released to production at any time.
3. Continuous Integration = A software development practice where contributors are integrating their work very frequently.

Continuous delivery means the code CAN be released at any time, while continuous deployment means it is released continuously.

# Part 2 Installing Jenkins

## Prerequisities

#### Ensure 8080 open and Install Java
netstat -anp | grep 8080 

sudo add-apt-repository ppa:openjdk-r/ppa
sudo apt-get update
sudo apt-get install openjdk-8-jdk

## Installing Jenkins

wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | sudo apt-key add -
sudo sh -c 'echo deb http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install jenkins
/etc/init.d/jenkins start

## Configuring from Wizard

1. http://localhost:8080
2. Installing setup plugins by clicking the button
3. Create first user by filling in the field

# Part 3 User Management and Security

To configure global security.

1. Manage Jenkins http://jenkinsurl/manage
2. Configure globcal security
3. Matrix based authentication
4. configure users/groups via matrix permissions read, write, etc

To add another user. 

1. Manage jenkins
2. Manage Users
3. Create User

Hovering over the relevant permission in the matrix will give you a breif overview if you are unsure of what it does.

# Part 4 Adding Slaves to the master

Multiple ways of adding slaves for different OS. Most common is SSH through Linux and Unix and Windows service for Windows.

To add a linux slave through ssh.

1. Ensure connectivity between slave and master (from master ping slave and vice versa)

## From the Slave

2. Install Java on Slave
3. Create the jenkins slave user
adduser jenkins
4. Swap to the slave user and create an sshkey
cd ~/.ssh/; ssh-keygen -t rsa -C "$myslave" -f "$myslave_rsa"
5. In the credentials manager for Jenkins http://jenkinsurl/credentials/
6. Create a credential for ssh user with private key

## From the Master/UI

7. Insert the SSH key either from file on the jenkins master or by pasting the key inside the credential.
8. In the node management for jenkins http://jenkinsurl/computer/
9. Add a new slave and join via ssh.
10. Select Relevant trust and add the Credential created before hand.

# Part 5 Integrating a Code repository

I will be using gitlab for the following instructions but most likely and decent code repo via ssh or api

1. Download the gitlab Jenkins plugin. 
2. Grab the API key from your gitlab user under settings
3. Go into configure system and scroll to the gitlab connection settings
4. Add your gitlab server in my case im using gitlabs provided repo so http://gitlab.com
5. Create a gitlab api token credential in Jenkins and paste your API key.
6. Test the connection
7. Grab your ssh key on your master and copy the public key to your github
8. Use the private key inside a jenkins credentials and you should be able to build code from gitlab now.

# Part 6 Installing, Removing, Upgrading and Downgrading plugins

1. All plugins can be managed from http://jenkinsurl/pluginManager/

## Installing

1. You can install a plugin by searching for the required plugin and clicking the install button

## Uninstall

2. You can uninstall a plugin by clicking the uninstall plugin. You may need to restart the jenkins daemon

## Upgrading

3. Upgrading can be done by clicking the updates tab and looking at all of your old versioned plugins. You can click download to star the update.

## Downgrading

4. Downgrading can be done by grabbing the old version from https://plugins.jenkins.io
5. Once downloaded you can click advanced and upload the .hpi file directly via upload plugin.
6. If upgraded from a previously installed version the plugin manager will track and you can downgrade from it.
