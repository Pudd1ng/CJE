Vagrant Details | Installing and Configuring Jenkins | Projects |

# Vagrant Details

The following Vagrant file will confugre three machines.
Your machine will need atleast 4gb of ram to run this succesfully.

To run clone this repo cd into it and run vagrant up. If the below private ips are not availble on your network ensure to update the vagrant file accordingly.

## How to access via ssh

Inside the root directory run the following command corrseponding to your target server

vagrant ssh master
vagrant ssh agent
vagrant ssh gitlab

## How to access via browser

Jenkins Master - http://10.0.1.103:8080
Jenkins Slave - ssh vagrant@10.0.1.104
Gitlab Server - http://10.0.1.105

## Your configuration

Some steps have been left out of the automation so that administartion tasks can be handled by the user. The reason for this is that in the CCJE some of these tasks may come up as questions. e.g. where is the default jenkins access key located?

### Jenkins Master configuration

1. Navigate to the above url 
2. Follow the standard configuration wizard

### Jenkins Slave configuration

1. This is hanlded under Installing and Configuring Jenkins

### Gitlab Server configuration

1. Although not in the CCJE still relevant skill to learn
2. Navigate to the above url and set a password for the default root user
3. Create a project for use with tht git integrations

Note: SSH keys will be handeld in Installing and configuring Jenkins
Note: The Jenkins CI integration is not availble in the Community Edition of Gitlab. You will need to set up your own webhook
